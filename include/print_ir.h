/*
    File:    print_ir.h
    Created: 23 April 2019 at 09:18 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef PRINT_IR_H
#define PRINT_IR_H
#   include "../include/intermediate_repres.h"
void print_ir(const lynx_ir::Command& buf);
#endif